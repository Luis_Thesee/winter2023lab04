import java.util.Scanner;
public class Toaster{
		
	private String brand;
	private int watts;
	private int slices;
	private String bread;
	
	
	public Toaster(String brand, int watts, int slices){
		this.brand = brand;
		this.watts = watts;
		this.slices = slices;
	}
	

	public boolean validSlices(){
		
		Scanner reader = new Scanner(System.in);
		
		if(this.slices > 0){
			return true;
		}
		else{
			while(this.slices <= 0){
				System.out.println("please enter a valid number of slices.");
				System.out.println("input the ammount of slices the toaster can hold");
				this.slices = Integer.parseInt(reader.nextLine());
			}
			return true;
		}
		//check and loop
		//is input postive? 
			//if yes, simply return input
			//else... loop and ask the user to keep inputing until the number is positive
			
		
	}
	

	
	
	public String getBrand(){
		return this.brand;
	}
	
	public int getWatts(){
		return this.watts;
	}
	
	public int getSlices(){
		return this.slices;
	}
	
	public String getBread(){
		return this.bread;
	}
		
		
	public void howManySlices(){
		System.out.println("this toaster can toast up to " + this.slices + " at the same time!");
	}
	
	public void power(){
		if (this.watts > 50000){
			System.out.println("this toaster will overheat!");
		}
		else {
			System.out.println("this toaster is fine, it wont overheat!");
		}
	}
	
	public void toastBread(){
		
		if (validSlices()){
			this.bread = "ur bread is now toasted";
		}
	}
}


/* System.out.println( */